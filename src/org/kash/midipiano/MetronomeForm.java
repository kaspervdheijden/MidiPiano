package org.kash.midipiano;

import java.io.Serial;

/**
 * This class represents the Metronome form class.
 */
final class MetronomeForm extends javax.swing.JDialog implements java.awt.event.ActionListener, javax.swing.event.ChangeListener
{
   private static final java.lang.String KEY_BEATCOUNT = "TXT_BEATCOUNT";
   private static final java.lang.String KEY_TITLE = "TXT_METRONOME";
   private static final java.lang.String KEY_TEMPO = "TXT_TEMPO";

   private static final int INITIAL_NUMBER_OF_BEATS = 4;
   private static final int MAX_NUMBER_OF_BEATS = 13;
   private static final int MIN_NUMBER_OF_BEATS = 2;

   @Serial
   private static final long serialVersionUID = 1L;

   private javax.swing.JToggleButton btnToggleMetronome;
   private javax.swing.JSpinner sprBeatCount;
   private javax.swing.JSpinner sprTempo;
   private Metronome metronome;
   private MidiPlayer player;
   private final int curProgram;

   private MetronomeForm(
       final javax.swing.JFrame parentOwner,
       final MidiPlayer midiPlayer,
       final java.util.ResourceBundle bundle
    ) {
      super(parentOwner, null, true);

      sprBeatCount = new javax.swing.JSpinner(new javax.swing.SpinnerNumberModel(INITIAL_NUMBER_OF_BEATS, MIN_NUMBER_OF_BEATS, MAX_NUMBER_OF_BEATS, 1));
      sprTempo     = new javax.swing.JSpinner(new javax.swing.SpinnerNumberModel(MidiPlayer.DEFAULT_TEMPO, MidiPlayer.MIN_TEMPO, MidiPlayer.MAX_TEMPO, 1));
      curProgram   = midiPlayer.getProgram();

      metronome = new Metronome(new java.awt.Color(230, 230, 230), INITIAL_NUMBER_OF_BEATS, midiPlayer);
      player    = midiPlayer;

      final javax.swing.JLabel lblBeatCount = new javax.swing.JLabel(bundle.getString(KEY_BEATCOUNT));
      final javax.swing.JLabel lblTempo     = new javax.swing.JLabel(bundle.getString(KEY_TEMPO));
      final javax.swing.GroupLayout layout  = new javax.swing.GroupLayout(getContentPane());
      final java.lang.String strTitle       = bundle.getString(KEY_TITLE);


      btnToggleMetronome = new javax.swing.JToggleButton(strTitle, false);
      metronome.setPreferredSize(new java.awt.Dimension(65, 65));
      metronome.setOpaque(false);

      getContentPane().setLayout(layout);
      layout.setHorizontalGroup(
         layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(metronome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE, java.lang.Short.MAX_VALUE)
               .addGroup(layout.createSequentialGroup()
                  .addComponent(lblTempo)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                  .addComponent(sprTempo, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addGap(18, 18, 18)
                  .addComponent(lblBeatCount)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(sprBeatCount, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 167, java.lang.Short.MAX_VALUE)
                  .addComponent(btnToggleMetronome)))
            .addContainerGap())
      );

      layout.setVerticalGroup(
         layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(layout.createSequentialGroup()
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, java.lang.Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
               .addComponent(lblTempo)
               .addComponent(sprTempo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(lblBeatCount)
               .addComponent(sprBeatCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(btnToggleMetronome))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(metronome, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
      );

      setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
      setResizable(false);
      setTitle(strTitle);
      player.stop();

      pack();
      setLocationRelativeTo(parentOwner);
   }

   /**
    * Displays the form. This method should be called when this form is to be displayed.
    */
   static void showForm(final javax.swing.JFrame parent, final MidiPlayer midiPlayer, final java.util.ResourceBundle bundle)
   {
      final MetronomeForm form = new MetronomeForm(parent, midiPlayer, bundle);

      form.btnToggleMetronome.addActionListener(form);
      form.sprBeatCount.addChangeListener(form);
      form.sprTempo.addChangeListener(form);

      form.setVisible(true);
   }

   @Override
   public void actionPerformed(final java.awt.event.ActionEvent e)
   {
      if (metronome.isRunning()) {
         metronome.stop();
      } else {
         metronome.start();
      }
   }

   @Override
   public void stateChanged(final javax.swing.event.ChangeEvent e)
   {
      if (e.getSource().equals(sprTempo)) {
         metronome.setTempo((Integer)sprTempo.getValue());
      } else if (e.getSource().equals(sprBeatCount)) {
         final int num = (Integer)sprBeatCount.getValue();
         int curCount  = metronome.getLength();

          if (num > curCount) {
            do {
               metronome.add();
            } while (++curCount != num);
         } else if (num != curCount) {
            while (curCount-- != num) {
               metronome.shift();
            }
         }

         repaint();
         metronome.revalidate();
         metronome.repaint();
      }
   }

   @Override
   public void dispose()
   {
      if (metronome != null) {
         metronome.dispose();
         metronome = null;
      }

      if (player != null) {
         player.setProgram(curProgram);
         player = null;
      }

      if (btnToggleMetronome != null) {
         btnToggleMetronome.removeActionListener(this);
         btnToggleMetronome = null;
      }

      if (sprBeatCount != null) {
         sprBeatCount.removeChangeListener(this);
         sprBeatCount = null;
      }

      if (sprTempo != null) {
         sprTempo.removeChangeListener(this);
         sprTempo = null;
      }

      super.dispose();
   }
}
