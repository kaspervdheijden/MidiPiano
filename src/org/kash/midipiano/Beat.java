package org.kash.midipiano;

import java.io.Serial;

/**
 * A beat represents a Beat for the Metronome.
 */
final class Beat extends javax.swing.JPanel implements java.awt.event.MouseListener
{
    private static final java.awt.Color CLR_ACCENT_NONE  = new java.awt.Color(160, 200, 150);
    private static final java.awt.Color CLR_ACCENT_MINOR = new java.awt.Color(225, 170, 0);
    private static final java.awt.Color CLR_MOUSEOVER    = new java.awt.Color(0xCC0066CC);
    private static final java.awt.Color CLR_ACCENT_MAJOR = java.awt.Color.ORANGE;
    private static final java.awt.Color CLR_HIGHLICHT    = java.awt.Color.PINK;

    @Serial
    private static final long serialVersionUID = 1L;

    static final int ACCENT_MAJOR = 1;
    static final int ACCENT_MINOR = 2;

    private boolean highlighted = false;
    private java.lang.String textCaption = "";
    private boolean hasMouse = false;
    private int state = 0;

    /**
     * Private constructor. Please create an instance through the factory method.
     *
     * @see Beat::create
     */
    private Beat()
    {
        super(true);

        setFont(new java.awt.Font(java.awt.Font.MONOSPACED, java.awt.Font.BOLD, 20));
        setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        setPreferredSize(new java.awt.Dimension(32, 42));
    }

    /**
     * Creates a Beat.
     *
     * @param beat The beat number.
     */
    static Beat create(final int beat)
    {
        final Beat retVal  = new Beat();

        retVal.textCaption = java.lang.String.valueOf(beat);
        retVal.addMouseListener(retVal);

        return retVal;
    }

    /**
     * Gets the MIDI note for this Beat. A different one is used for different accents.
     *
     * @return The MIDI note to use for this beat.
     */
    int getMIDINote()
    {
        switch (state) {
            case ACCENT_MAJOR -> {
                return 80;
            }
            case ACCENT_MINOR -> {
                return 72;
            }
            default -> {
                return 75;
            }
        }
    }

    /**
     * Toggles the highlight state for thias beat. A beat is highlighted
     * Whenever the Metronome playes this beat.
     *
     * @param newHighlight The new Highlight state.
     */
    void setHighlighted(final boolean newHighlight)
    {
        if (highlighted != newHighlight) {
            highlighted = newHighlight;
            repaint();
        }
    }

    /**
     * Removes listeners, and releases any resources used by this class.
     */
    void dispose()
    {
        removeMouseListener(this);
        textCaption = null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void paintComponent(final java.awt.Graphics g)
    {
        final java.awt.Graphics2D gx = (java.awt.Graphics2D)g;
        final int w                  = gx.getFontMetrics(getFont()).stringWidth(textCaption);
        final int height             = getHeight();
        final int width              = getWidth();

        gx.setRenderingHint(java.awt.RenderingHints.KEY_ANTIALIASING, java.awt.RenderingHints.VALUE_ANTIALIAS_ON);

        super.paintComponent(g);
        gx.setColor(getParent().getBackground());
        gx.fillRect(0, 0, width, height);

        if (highlighted) {
            gx.setColor(CLR_HIGHLICHT);
        } else if (state == ACCENT_MAJOR) {
            gx.setColor(CLR_ACCENT_MAJOR);
        } else if (state == ACCENT_MINOR) {
            gx.setColor(CLR_ACCENT_MINOR);
        } else {
            gx.setColor(CLR_ACCENT_NONE);
        }

        gx.fillOval(0, 10, width - 1, height - 11);
        gx.setColor(java.awt.Color.DARK_GRAY);
        gx.drawOval(0, 10, width - 1, height - 11);

        if (hasMouse) {
            gx.setColor(CLR_MOUSEOVER);
        }

        gx.drawString(textCaption, (width - w) / 1.95f, 32);
    }

    @Override
    public void mouseClicked(final java.awt.event.MouseEvent e)
    {
        state = ++state % 3;
        repaint();
    }

    @Override
    public void mouseEntered(final java.awt.event.MouseEvent e)
    {
        if (! hasMouse) {
            hasMouse = true;
            repaint();
        }
    }

    @Override
    public void mouseExited(final java.awt.event.MouseEvent e)
    {
        if (hasMouse) {
            hasMouse = false;
            repaint();
        }
    }

    @Override
    public void mousePressed(final java.awt.event.MouseEvent e)
    {
    }

    @Override
    public void mouseReleased(final java.awt.event.MouseEvent e)
    {
    }
}
