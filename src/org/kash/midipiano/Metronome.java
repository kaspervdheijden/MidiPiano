package org.kash.midipiano;

import java.io.Serial;

/**
 * This class representates a Metronome.
 */
final class Metronome extends javax.swing.JPanel implements java.awt.event.ActionListener
{
    private static final int DEFAULT_PROGRAM = 115;

    @Serial
    private static final long serialVersionUID = 1L;

    private javax.swing.Timer tmr;
    private MidiPlayer player;
    private Beat lastBeat = null;
    private int curIndex = -1;
    private int count = 0;

    /**
     * @param backgroundColor The background color to be used.
     * @param numBeats        The initial number of beats this metronome should have.
     * @param midiPlayer      The midiPlayer to use for sound output from this metronome.
     */
    Metronome(
        final java.awt.Color backgroundColor,
        final int numBeats,
        final MidiPlayer midiPlayer
    ) {
        super(true);

        setBackground(backgroundColor);
        for (int i = 0; i < numBeats; ++i) {
            add();
        }

        midiPlayer.setProgram(DEFAULT_PROGRAM);

        tmr    = new javax.swing.Timer(500, null);
        player = midiPlayer;
    }

    /**
     * Tells if the metronome is running.
     *
     * @return true if this metronome is running, false if not.
     */
     boolean isRunning()
    {
        return tmr.isRunning();
    }

    /**
     * Returns the number of beats played by the metronome.
     *
     * @return The number of beats played by the metronome.
     */
    int getLength()
    {
        return count;
    }

    /**
     * Adds a Beat to the metronome.
     */
    void add()
    {
        add(Beat.create(++count));
    }

    /**
     * Removes the last Beat from this metronome.
     * The removed Beat is already disposed.
     */
    void shift()
    {
        final java.awt.Component comp = getComponent(count - 1);

        if (comp instanceof Beat) {
            ((Beat)comp).dispose();

            remove(comp);
            --count;
        }
    }

    /**
     * Sets the metronome tempo in BPM.
     *
     * @param tempo The new tempo in BPM.
     */
    void setTempo(final int tempo)
    {
        tmr.setDelay(60000 / tempo);
    }

    /**
     * Starts this metronome if not already started.
     */
    void start()
    {
        if (! isRunning()) {
            tmr.addActionListener(this);
            tmr.start();
        }
    }

    /**
     * Stops this metronome, if it is running.
     */
    void stop()
    {
        if (! isRunning()) {
            return;
        }

        tmr.removeActionListener(this);
        curIndex = -1;
        tmr.stop();

        if (lastBeat != null) {
            player.noteOff(lastBeat.getMIDINote());
        }

        for (final java.awt.Component comp : getComponents()) {
            if (comp instanceof Beat) {
                ((Beat)comp).setHighlighted(false);
            }
        }
    }

    /**
     * Releases all underlying resources used by this class.
     */
    void dispose()
    {
        stop();

        for (final java.awt.Component comp : getComponents()) {
            if (comp instanceof Beat) {
                ((Beat) comp).dispose();
            }
        }

        if (tmr != null) {
            tmr.removeActionListener(this);
            tmr = null;
        }

        lastBeat = null;
        player   = null;
        curIndex = 0;
    }

    @Override
    protected void paintComponent(final java.awt.Graphics g)
    {
        final java.awt.Graphics2D gx = (java.awt.Graphics2D) g;
        final int height             = getHeight();
        final int width              = getWidth();

        gx.setRenderingHint(java.awt.RenderingHints.KEY_ANTIALIASING, java.awt.RenderingHints.VALUE_ANTIALIAS_ON);
        gx.setColor(getBackground());

        gx.fillRoundRect(0, 0, width - 2, height - 8, 16, 16);
        gx.setColor(java.awt.Color.GRAY);
        gx.drawRoundRect(0, 0, width - 2, height - 8, 16, 16);
    }

    @Override
    public void actionPerformed(final java.awt.event.ActionEvent e)
    {
        curIndex = ++curIndex % count;

        if (lastBeat != null) {
            lastBeat.setHighlighted(false);
            player.noteOff(lastBeat.getMIDINote());
        }

        lastBeat = (Beat)getComponent(curIndex);
        lastBeat.setHighlighted(true);

        player.noteOn(lastBeat.getMIDINote());

        repaint();
    }
}
