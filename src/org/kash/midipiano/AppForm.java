package org.kash.midipiano;

import javax.sound.midi.InvalidMidiDataException;
import javax.swing.*;
import java.io.IOException;
import java.io.Serial;

/**
 * The application main form class.
 */
final class AppForm extends javax.swing.JFrame implements javax.swing.event.ChangeListener,
                                                          java.awt.event.ActionListener,
                                                          java.awt.event.ItemListener,
                                                          IMidiAction
{
    @Serial
    private static final long serialVersionUID = 1L;

    // UI label texts..;
    private static final java.lang.String TXT_DIALOG_FILTER = "TXT_DIALOG_FILTER";
    private static final java.lang.String TXT_INSTRUMENT = "TXT_INSTRUMENT";
    private static final java.lang.String TXT_TEMPO = "TXT_TEMPO";

    // UI command texts..;
    private static final java.lang.String CMD_METRONOME = "CMD_METRONOME";
    private static final java.lang.String CMD_PAUSE = "CMD_PAUSE";
    private static final java.lang.String CMD_ABOUT = "CMD_ABOUT";
    private static final java.lang.String CMD_FILE = "CMD_FILE";
    private static final java.lang.String CMD_OPEN = "CMD_OPEN";
    private static final java.lang.String CMD_EXIT = "CMD_EXIT";
    private static final java.lang.String CMD_PLAY = "CMD_PLAY";
    private static final java.lang.String CMD_STOP = "CMD_STOP";

    private static final java.lang.String CBO_INSTRUMENTS = "CBO_INSTRUMENTS";
    private static final java.lang.String SLDR_POSITION = "SLDR_POSITION";
    private static final java.lang.String TXT_PERPETUAL = "TXT_PERPETUAL";
    private static final java.lang.String TXT_POSITION = "TXT_POSITION";
    private static final java.lang.String SPR_TEMPO = "SPR_TEMPO";

    // Midi file extension..;
    private static final java.lang.String MIDI_EXTENSION = ".mid";

    // Private variables..;
    private java.util.HashMap<java.lang.String, javax.swing.JComponent> components = null;
    private javax.swing.JFileChooser dialog = null;
    private java.util.ResourceBundle bundle = null;
    private javax.swing.Timer timer = null;
    private MidiPlayer player = null;
    private Piano piano = null;

    private boolean blckInstruments = false;
    private boolean blckPosition = false;
    private boolean blckTempo = false;

    private AppForm(final java.lang.String strTitle)
    {
        super(strTitle);
    }

    /**
     * Displays the form. This method should be called when this form is to be displayed.
     */
    static void showForm(
        final java.lang.String strTitle,
        final java.util.ResourceBundle resourceBundle,
        final MidiPlayer midiPlayer,
        final java.io.File file
    ) {
        final int NUM_OCTAVES = 6; // Number of octaves used on keyboard.

        final AppForm form = new AppForm(strTitle);

        final java.util.HashMap<java.lang.String, javax.swing.JComponent> comps = new java.util.HashMap<>();
        final javax.swing.JFileChooser dlg                                         = new javax.swing.JFileChooser();

        form.setLocale(resourceBundle.getLocale());

        dlg.setFileFilter(new javax.swing.filechooser.FileNameExtensionFilter(resourceBundle.getString(TXT_DIALOG_FILTER), MIDI_EXTENSION.substring(1)));
        dlg.setLocale(resourceBundle.getLocale());

        dlg.setAcceptAllFileFilterUsed(false);
        dlg.setMultiSelectionEnabled(false);
        dlg.setDragEnabled(true);

        final java.awt.Dimension oScreenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        final java.awt.Container c           = form.getContentPane();

        c.add(form.piano = new Piano(NUM_OCTAVES, form, 5, 30));
        form.timer          = new javax.swing.Timer(20, form);
        form.player      = midiPlayer;

        midiPlayer.setUIListener(form);
        form.bundle     = resourceBundle;
        form.components = comps;
        form.dialog     = dlg;

        final int w = 5 + form.piano.getWidth();
        form.setBounds((oScreenSize.width - w) / 2, (int) ((oScreenSize.height - 190) / 2.6), w, 190);
        form.setResizable(false);

        form.setIconImage(java.awt.Toolkit.getDefaultToolkit().createImage(new byte[] {-119, 80, 78, 71, 13, 10, 26, 10, 0,
            0, 0, 13, 73, 72, 68, 82, 0, 0, 0, 16, 0, 0, 0, 16, 8, 3, 0, 0, 0, 40, 45, 15, 83, 0, 0, 0, 25, 116, 69, 88, 116,
            83, 111, 102, 116, 119, 97, 114, 101, 0, 65, 100, 111, 98, 101, 32, 73, 109, 97, 103, 101, 82, 101, 97, 100, 121,
            113, -55, 101, 60, 0, 0, 3, 102, 105, 84, 88, 116, 88, 77, 76, 58, 99, 111, 109, 46, 97, 100, 111, 98, 101, 46,
            120, 109, 112, 0, 0, 0, 0, 0, 60, 63, 120, 112, 97, 99, 107, 101, 116, 32, 98, 101, 103, 105, 110, 61, 34, -17,
            -69, -65, 34, 32, 105, 100, 61, 34, 87, 53, 77, 48, 77, 112, 67, 101, 104, 105, 72, 122, 114, 101, 83, 122, 78,
            84, 99, 122, 107, 99, 57, 100, 34, 63, 62, 32, 60, 120, 58, 120, 109, 112, 109, 101, 116, 97, 32, 120, 109, 108,
            110, 115, 58, 120, 61, 34, 97, 100, 111, 98, 101, 58, 110, 115, 58, 109, 101, 116, 97, 47, 34, 32, 120, 58, 120,
            109, 112, 116, 107, 61, 34, 65, 100, 111, 98, 101, 32, 88, 77, 80, 32, 67, 111, 114, 101, 32, 53, 46, 48, 45, 99,
            48, 54, 48, 32, 54, 49, 46, 49, 51, 52, 55, 55, 55, 44, 32, 50, 48, 49, 48, 47, 48, 50, 47, 49, 50, 45, 49, 55,
            58, 51, 50, 58, 48, 48, 32, 32, 32, 32, 32, 32, 32, 32, 34, 62, 32, 60, 114, 100, 102, 58, 82, 68, 70, 32, 120,
            109, 108, 110, 115, 58, 114, 100, 102, 61, 34, 104, 116, 116, 112, 58, 47, 47, 119, 119, 119, 46, 119, 51, 46,
            111, 114, 103, 47, 49, 57, 57, 57, 47, 48, 50, 47, 50, 50, 45, 114, 100, 102, 45, 115, 121, 110, 116, 97, 120,
            45, 110, 115, 35, 34, 62, 32, 60, 114, 100, 102, 58, 68, 101, 115, 99, 114, 105, 112, 116, 105, 111, 110, 32,
            114, 100, 102, 58, 97, 98, 111, 117, 116, 61, 34, 34, 32, 120, 109, 108, 110, 115, 58, 120, 109, 112, 77, 77,
            61, 34, 104, 116, 116, 112, 58, 47, 47, 110, 115, 46, 97, 100, 111, 98, 101, 46, 99, 111, 109, 47, 120, 97,
            112, 47, 49, 46, 48, 47, 109, 109, 47, 34, 32, 120, 109, 108, 110, 115, 58, 115, 116, 82, 101, 102, 61, 34,
            104, 116, 116, 112, 58, 47, 47, 110, 115, 46, 97, 100, 111, 98, 101, 46, 99, 111, 109, 47, 120, 97, 112, 47,
            49, 46, 48, 47, 115, 84, 121, 112, 101, 47, 82, 101, 115, 111, 117, 114, 99, 101, 82, 101, 102, 35, 34, 32,
            120, 109, 108, 110, 115, 58, 120, 109, 112, 61, 34, 104, 116, 116, 112, 58, 47, 47, 110, 115, 46, 97, 100,
            111, 98, 101, 46, 99, 111, 109, 47, 120, 97, 112, 47, 49, 46, 48, 47, 34, 32, 120, 109, 112, 77, 77, 58, 79,
            114, 105, 103, 105, 110, 97, 108, 68, 111, 99, 117, 109, 101, 110, 116, 73, 68, 61, 34, 117, 117, 105, 100,
            58, 102, 97, 102, 53, 98, 100, 100, 53, 45, 98, 97, 51, 100, 45, 49, 49, 100, 97, 45, 97, 100, 51, 49, 45,
            100, 51, 51, 100, 55, 53, 49, 56, 50, 102, 49, 98, 34, 32, 120, 109, 112, 77, 77, 58, 68, 111, 99, 117, 109,
            101, 110, 116, 73, 68, 61, 34, 120, 109, 112, 46, 100, 105, 100, 58, 52, 54, 69, 50, 69, 53, 52, 49, 66, 57,
            67, 56, 49, 49, 68, 70, 56, 57, 65, 68, 69, 51, 51, 53, 56, 52, 68, 56, 67, 68, 55, 54, 34, 32, 120, 109, 112,
            77, 77, 58, 73, 110, 115, 116, 97, 110, 99, 101, 73, 68, 61, 34, 120, 109, 112, 46, 105, 105, 100, 58, 52, 54,
            69, 50, 69, 53, 52, 48, 66, 57, 67, 56, 49, 49, 68, 70, 56, 57, 65, 68, 69, 51, 51, 53, 56, 52, 68, 56, 67, 68,
            55, 54, 34, 32, 120, 109, 112, 58, 67, 114, 101, 97, 116, 111, 114, 84, 111, 111, 108, 61, 34, 65, 100, 111, 98,
            101, 32, 80, 104, 111, 116, 111, 115, 104, 111, 112, 32, 67, 83, 53, 32, 87, 105, 110, 100, 111, 119, 115, 34,
            62, 32, 60, 120, 109, 112, 77, 77, 58, 68, 101, 114, 105, 118, 101, 100, 70, 114, 111, 109, 32, 115, 116, 82,
            101, 102, 58, 105, 110, 115, 116, 97, 110, 99, 101, 73, 68, 61, 34, 120, 109, 112, 46, 105, 105, 100, 58, 67,
            49, 69, 53, 70, 55, 51, 68, 67, 56, 66, 57, 68, 70, 49, 49, 66, 55, 66, 66, 66, 57, 49, 57, 56, 66, 52, 51,
            49, 50, 50, 65, 34, 32, 115, 116, 82, 101, 102, 58, 100, 111, 99, 117, 109, 101, 110, 116, 73, 68, 61, 34,
            117, 117, 105, 100, 58, 102, 97, 102, 53, 98, 100, 100, 53, 45, 98, 97, 51, 100, 45, 49, 49, 100, 97, 45, 97,
            100, 51, 49, 45, 100, 51, 51, 100, 55, 53, 49, 56, 50, 102, 49, 98, 34, 47, 62, 32, 60, 47, 114, 100, 102, 58,
            68, 101, 115, 99, 114, 105, 112, 116, 105, 111, 110, 62, 32, 60, 47, 114, 100, 102, 58, 82, 68, 70, 62, 32, 60,
            47, 120, 58, 120, 109, 112, 109, 101, 116, 97, 62, 32, 60, 63, 120, 112, 97, 99, 107, 101, 116, 32, 101, 110,
            100, 61, 34, 114, 34, 63, 62, -70, 55, -20, -58, 0, 0, 1, 83, 80, 76, 84, 69, -4, -4, -4, -6, -6, -6, -3, -3,
            -3, -106, -106, -106, -23, -23, -23, -17, -17, -17, -42, -42, -41, -62, -62, -62, -63, -63, -63, -68, -68, -68,
            -29, -29, -29, -86, -86, -86, 125, 125, 125, 124, 124, 124, -55, -55, -55, 90, 89, 91, 91, 91, 91, -43, -43,
            -43, -120, -121, -121, -22, -22, -21, -41, -41, -41, -77, -77, -77, 71, 71, 71, 69, 69, 69, -35, -34, -34, -24,
            -24, -24, 63, 63, 63, -79, -79, -77, -18, -18, -18, 102, 102, 99, -76, -76, -76, -118, -118, -119, 100, 100,
            100, 76, 76, 75, -52, -52, -52, -91, -91, -92, 94, 94, 95, -46, -46, -47, 45, 46, 44, 31, 31, 30, 127, 127, 127,
            -20, -22, 100, 100, 103, -15, -15, -15, -58, -58, -59, -27, -27, -26, -112, -112, -112, 88, 88, 87, -22, -22,
            -102, -102, -102, -33, -33, -33, -19, -19, -18, -57, -57, -54, 65, 64, 66, -53, -53, -53, -40, -40, -40, -21,
            -22, -15, -15, -14, 44, 44, 44, 122, 122, 122, 94, 94, 93, 13, 14, 13, -124, -124, -124, -125, -125, -125, -120,
            -120, -119, -50, -50, -50, -13, -13, -13, -44, -44, -44, -71, -71, -71, 82, 84, 87, -8, -8, -8, -6, -7, -6, -39,
            -38, -37, -46, -46, -46, -73, -73, -74, -115, -115, -115, 93, 93, 91, 107, 108, 103, -14, -14, -14, 117, 118, 113,
            -89, -89, -89, -25, -25, -25, -32, -32, -32, -28, -28, -28, -87, -88, -90, -17, -17, -16, -71, -71, -72, -85, -85,
            -85, -9, -9, -9, -122, -122, -120, -56, -56, -56, 68, 68, 68, -120, -119, -122, -48, -48, -48, -98, -98, -98, -7,
            -7, -7, -90, -90, -90, -12, -12, -12, 125, 125, 124, 104, 104, 106, -5, -5, -5, 89, 89, 90, -82, -82, -82, 83, 83,
            86, -29, -29, -28, -97, -96, -99, 68, 67, 72, -7, -7, -8, 12, 11, 10, 110, 110, 110, -2, -2, -2, 0, 0, 0, -1, -1,
            -1, -1, -1, -1, -102, 83, -55, 110, 0, 0, 0, 113, 116, 82, 78, 83, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 41, 76, -64, 101, 0, 0, 0, -42, 73, 68, 65, 84, 120, -38, 98,
            40, -16, 84, 45, -128, 0, -90, 24, 16, -55, 80, -64, -27, -59, 5, 17, 8, 23, 96, 2, 11, 48, -122, 49, 67, 4, 88, 69,
            -7, -64, 2, 5, -36, 86, 16, -127, 0, 22, 14, -80, 64, -66, -126, 19, -120, -85, 37, -25, -64, 22, 2, 22, -120, -44,
            13, 100, 53, -56, -49, 23, -49, -77, 99, 2, 27, -102, 43, -58, 46, -107, 98, -99, 81, 32, -61, -103, 12, -30, -118,
            48, -80, 88, 74, -85, 69, 27, 39, -7, -126, 100, -77, 66, -51, 108, 25, -76, -43, 57, 50, 19, -107, 121, -51, -127,
            -78, -63, 81, 108, 22, -102, 12, -7, -122, 62, 58, -4, 66, 60, -7, 12, -2, 54, 105, -126, 5, -7, -100, 12, 5, 12, 17,
            -78, -34, 57, -116, 5, 122, 121, -18, -71, 18, -7, -87, -79, 64, 119, 20, -40, -85, 56, 23, 20, 72, 122, 20, -28, 6,
            37, -28, 49, -125, 4, -28, -3, -40, 11, 10, -36, -108, 92, -77, 77, 11, -30, 114, 65, 2, -71, -62, -71, 5, 5, -116,
            -15, -114, 26, 70, 16, -89, -125, -127, 126, -98, 98, 126, 65, 1, -110, -128, -119, 75, 58, -124, 1, 16, 96, 0, 28,
            -113, 80, -41, -59, 111, 57, 92, 0, 0, 0, 0, 73, 69, 78, 68, -82, 66, 96, -126}));
        form.setDefaultCloseOperation(javax.swing.JFrame.DISPOSE_ON_CLOSE);
        form.setLayout(null);

        final javax.swing.JMenu menuFile = new javax.swing.JMenu(resourceBundle.getString(CMD_FILE));
        for (final java.lang.String nme : (new java.lang.String[] {CMD_OPEN, CMD_METRONOME, null, CMD_ABOUT, null, CMD_EXIT})) {
            if (null == nme) {
                menuFile.addSeparator();
            } else {
                final javax.swing.JMenuItem menu = new javax.swing.JMenuItem(resourceBundle.getString(nme));

                switch (nme) {
                    case CMD_OPEN -> {
                        menu.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
                        menu.setMnemonic(java.awt.event.KeyEvent.VK_O);
                    }
                    case CMD_ABOUT -> {
                        final char chrAcc = menu.getText().charAt(0);
                        menu.setAccelerator(javax.swing.KeyStroke.getKeyStroke(chrAcc, java.awt.event.InputEvent.CTRL_MASK));
                        menu.setMnemonic(chrAcc);
                    }
                    case CMD_EXIT -> {
                        menu.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
                        menu.setMnemonic(menu.getText().charAt(1));
                    }
                    case CMD_METRONOME -> {
                        menu.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F5, 0));
                        menu.setMnemonic(menu.getText().charAt(0));
                    }
                }

                menu.addActionListener(form);
                comps.put(nme, menu);
                menu.setName(nme);
                menuFile.add(menu);
            }
        }

        final javax.swing.JMenuBar mnuBar = new javax.swing.JMenuBar();
        menuFile.setMnemonic(menuFile.getText().charAt(0));
        form.setJMenuBar(mnuBar);
        mnuBar.add(menuFile);

        final javax.swing.JToolBar tlBar = new javax.swing.JToolBar();
        tlBar.setBounds(0, 0, form.getWidth(), 30);
        tlBar.setFloatable(false);

        for (final java.lang.String name : (new java.lang.String[] {CMD_PLAY, CMD_PAUSE, CMD_STOP})) {
            final java.net.URL img            = Application.class.getResource("resources/" + name.substring(4).toLowerCase() + ".png");
            final java.lang.String tooltip = resourceBundle.getString(name);
            final javax.swing.JButton btn  = new javax.swing.JButton();

            btn.setFocusable(false);
            btn.setEnabled(false);
            btn.setName(name);

            if (img != null) {
                btn.setIcon(new javax.swing.ImageIcon(img));
            } else {
                btn.setText(tooltip);
            }

            btn.addActionListener(form);
            btn.setToolTipText(tooltip);
            comps.put(name, btn);
            tlBar.add(btn);
        }

        tlBar.addSeparator();

        final javax.swing.JCheckBox chkPerpetual = new javax.swing.JCheckBox(resourceBundle.getString(TXT_PERPETUAL), false);
        chkPerpetual.addActionListener(form);

        tlBar.add(chkPerpetual);
        tlBar.addSeparator();

        final javax.swing.JComboBox<java.lang.String> cboInstruments = new javax.swing.JComboBox<>(midiPlayer.getInstruments());
        cboInstruments.setSelectedIndex(MidiPlayer.DEFAULT_INSTRUMENT);
        cboInstruments.setMaximumSize(cboInstruments.getMinimumSize());
        comps.put(CBO_INSTRUMENTS, cboInstruments);
        cboInstruments.addItemListener(form);
        cboInstruments.setFocusable(false);

        final javax.swing.JLabel lblInstrument = new javax.swing.JLabel(resourceBundle.getString(TXT_INSTRUMENT));
        lblInstrument.setBorder(new javax.swing.border.EmptyBorder(0, 6, 0, 4));
        lblInstrument.setLabelFor(cboInstruments);

        tlBar.add(lblInstrument);
        tlBar.add(cboInstruments);

        final javax.swing.JSpinner sprTempo = new javax.swing.JSpinner(new javax.swing.SpinnerNumberModel(MidiPlayer.DEFAULT_TEMPO, MidiPlayer.MIN_TEMPO, MidiPlayer.MAX_TEMPO, 1));
        ((javax.swing.JSpinner.NumberEditor) sprTempo.getEditor()).getTextField().setEditable(false);
        sprTempo.setMaximumSize(sprTempo.getPreferredSize());
        sprTempo.addChangeListener(form);
        comps.put(SPR_TEMPO, sprTempo);
        sprTempo.setFocusable(false);
        sprTempo.setEnabled(false);

        final javax.swing.JLabel lblTempo = new javax.swing.JLabel(resourceBundle.getString(TXT_TEMPO));
        lblTempo.setBorder(new javax.swing.border.EmptyBorder(0, 8, 0, 4));
        lblTempo.setLabelFor(sprTempo);

        final javax.swing.JLabel lblMargin = new javax.swing.JLabel();
        lblMargin.setPreferredSize(new java.awt.Dimension(4, 0));

        tlBar.add(lblTempo);
        tlBar.add(sprTempo);
        tlBar.add(lblMargin);
        tlBar.addSeparator();

        final javax.swing.JSlider slider = new javax.swing.JSlider(javax.swing.JSlider.HORIZONTAL, 0, 0, 0);
        slider.setPreferredSize(new java.awt.Dimension(620, 25));
        slider.addChangeListener(form);
        comps.put(SLDR_POSITION, slider);
        slider.setFocusable(false);
        slider.setEnabled(false);

        final javax.swing.JLabel lblPosition = new javax.swing.JLabel(form.player.getPositionTimeDisplayString());
        lblPosition.setBorder(new javax.swing.border.EmptyBorder(0, 4, 0, 16));
        comps.put(TXT_POSITION, lblPosition);

        tlBar.add(slider);
        tlBar.add(lblPosition);
        c.add(tlBar);

        form.setVisible(true);
        if (file != null) {
            form.openFile(file);
        }
    }

    /**
     * Starts playback on the player if possible.
     */
    private void playPlayer()
    {
        if (player != null && player.isPlayable()) {
            player.play();
        }
    }

    /**
     * Opens a specific file for MIDI playback.
     *
     * @param file The MIDI file to open and play.
     */
    private void openFile(final java.io.File file)
    {
        java.io.File fileToOpen = file;

        if (file == null) {
            dialog.setSelectedFile(new java.io.File(""));

            if (javax.swing.JFileChooser.APPROVE_OPTION == dialog.showOpenDialog(this)) {
                fileToOpen = dialog.getSelectedFile();
            }
        }

        if (fileToOpen != null && fileToOpen.isFile() && fileToOpen.exists()) {
            java.lang.Exception error = null;

            try {
                player.open(fileToOpen);
            } catch (final InvalidMidiDataException | IOException err) {
                error = err;
            }

            if (error == null) {
                return;
            }

            java.lang.String errorMsg = error.getLocalizedMessage().trim();
            if (errorMsg.isEmpty()) {
                errorMsg = bundle.getString(Application.ERR_INVALID_MIDI_DATA);
            }

            Application.handleException(error);
            Application.showErrorMessage(this, errorMsg);
        }
    }

    /**
     * {@inheritDoc}
     *
     * Handles a user change on the instrument combobox.
     */
    @Override
    public void itemStateChanged(final java.awt.event.ItemEvent e)
    {
        if (! blckInstruments && e.getStateChange() == java.awt.event.ItemEvent.SELECTED) {
            final int instrument = ((javax.swing.JComboBox<?>)e.getSource()).getSelectedIndex();

            // Set the player instrument to the requested instrument program..;
            player.setInstrument(instrument);
        }
    }

    /**
     * {@inheritDoc}
     *
     *    1. Handles a change on the position slider. We should update the position.
     *    2. Handles a change on the tempo spinner. We should set te requested tempo.
     */
    @Override
    public void stateChanged(final javax.swing.event.ChangeEvent e)
    {
        final java.lang.Object src = e.getSource();

        if (src instanceof javax.swing.JSlider) {
            if (! blckPosition) {
                // User changed the position slider. Set the playback position to the requested position..;

                player.setPosition(((javax.swing.JSlider)src).getValue());
            }
        } else if (src instanceof javax.swing.JSpinner) {
            // The user changed the tempo on the spinner. If we're not currently playing, set the tempo.
            // Because the tempo changes, the playback duration should also change. Therefore, we should
            // also set the playback length description.
            if (! blckTempo && ! player.isPlaying()) {
                player.setTempo((Integer) ((javax.swing.JSpinner)src).getValue());
                setSliderPosition(0);
            }
        }
    }

    /**
     * Sets the slider position to the requested position, and optionally sets the
     * position description text.
     *
     * @param pos The position to set.
     */
    private void setSliderPosition(final int pos)
    {
        final javax.swing.JSlider slider = ((javax.swing.JSlider)components.get(SLDR_POSITION));

        ((javax.swing.JLabel)components.get(TXT_POSITION)).setText(player.getPositionTimeDisplayString());

        slider.setMaximum(player.getLength());
        slider.setExtent(slider.getMaximum() / 25);
        slider.setValue(pos);
    }

    @Override
    public void playerFileOpenened(final java.io.File file)
    {
        setTitle(Application.getName() + " - " + file.getAbsolutePath());
        programmaticUpdateTempo(player.getTempo());

        setSliderPosition(0);
        if (player.isPlayable()) {
            player.schedulePlaybackStart();
        }
    }

    /**
     * {@inheritDoc}
     *
     * Processes incomming MIDI events during playback only.
     */
    @Override
    public void playbackMIDIEvent(final int type, final int value)
    {
        switch (type) {
            case IMidiAction.MIDI_TEMPO_CHANGE   -> programmaticUpdateTempo(value);
            case IMidiAction.MIDI_NOTE_OFF       -> piano.releaseKey(value);
            case IMidiAction.MIDI_NOTE_ON        -> piano.pressKey(value);
            case IMidiAction.MIDI_PROGRAM_CHANGE -> {
                final javax.swing.JComboBox<?> cbo = (javax.swing.JComboBox<?>)components.get(CBO_INSTRUMENTS);

                if (-1 < value && value != cbo.getSelectedIndex() && value < cbo.getItemCount()) {
                    blckInstruments = true;
                    //cbo.removeItemListener(this);
                    cbo.setSelectedIndex(value);
                    blckInstruments = false;
                    //cbo.addItemListener(this);
                }
            }
        }
    }

    private void programmaticUpdateTempo(final int value)
    {
        final javax.swing.JSpinner sprTempo = (javax.swing.JSpinner)components.get(SPR_TEMPO);

        if (value != (Integer)sprTempo.getValue()) {
            //sprTempo.removeChangeListener(this);
            blckTempo = true;
            sprTempo.setValue(value);
            blckTempo = false;
            //sprTempo.addChangeListener(this);
        }
    }

    /**
     * {@inheritDoc}
     *
     * Callback to notify this UI form about any changes to the underlying MIDI player.
     */
    @Override
    public void playerStateChange(final int type)
    {
        switch (type) {
            case MidiPlayer.STATE_PAUSED -> {
                if (timer.isRunning()) {
                    timer.stop();
                }

                piano.releaseAllKeys();

                components.get(CMD_PAUSE).setEnabled(false);
                components.get(SPR_TEMPO).setEnabled(true);
                components.get(CMD_PLAY).setEnabled(true);
                components.get(CMD_STOP).setEnabled(true);
            }
            case MidiPlayer.STATE_PLAYING -> {
                components.get(CBO_INSTRUMENTS).setEnabled(false);
                components.get(SPR_TEMPO).setEnabled(false);
                components.get(CMD_PLAY).setEnabled(false);
                components.get(CMD_PAUSE).setEnabled(true);
                components.get(CMD_STOP).setEnabled(true);

                timer.start();
            }
            case MidiPlayer.STATE_IDLE -> {
                if (timer.isRunning()) {
                    timer.stop();
                }

                piano.releaseAllKeys();

                final boolean isPlayable = player.isPlayable();

                components.get(SLDR_POSITION).setEnabled(isPlayable);
                components.get(SPR_TEMPO).setEnabled(isPlayable);
                components.get(CBO_INSTRUMENTS).setEnabled(true);
                components.get(CMD_PLAY).setEnabled(isPlayable);
                components.get(CMD_PAUSE).setEnabled(false);
                components.get(CMD_STOP).setEnabled(false);

                setSliderPosition(player.getPosition());
            }
        }
    }

    @Override
    public void pianoMousePressedEvent(final int note)
    {
        if (player != null) {
            player.noteOn(note);
        }
    }

    @Override
    public void pianoMouseReleasedEvent(final int note)
    {
        if (player != null) {
            player.noteOff(note);
        }
    }

    @Override
    public void actionPerformed(final java.awt.event.ActionEvent e)
    {
        final java.lang.Object source = e.getSource();

        if (timer.equals(source)) {
            final javax.swing.JSlider slider = ((javax.swing.JSlider)components.get(SLDR_POSITION));

            ((javax.swing.JLabel)components.get(TXT_POSITION)).setText(player.getPositionTimeDisplayString());
            //slider.removeChangeListener(this);
            blckPosition = true;
            slider.setValue(player.getPosition());
            blckPosition = false;
            //slider.addChangeListener(this);
        } else if (source instanceof javax.swing.JCheckBox) {
            player.setPerpetual(((javax.swing.JCheckBox)source).isSelected());
        } else if (source instanceof javax.swing.JComponent) {
            final java.lang.String commandName = ((javax.swing.JComponent)source).getName();

            switch (commandName) {
                case CMD_METRONOME -> MetronomeForm.showForm(this, player, bundle);
                case CMD_EXIT      -> dispose();
                case CMD_OPEN      -> openFile(null);
                case CMD_ABOUT     -> AboutForm.showForm(this, bundle);
                case CMD_PLAY      -> playPlayer();
                case CMD_STOP      -> player.stop();
                case CMD_PAUSE     -> player.pause();
            }
        }
    }

    @Override
    public void dispose()
    {
        if (timer != null) {
            timer.removeActionListener(this);
            if (timer.isRunning()) {
                timer.stop();
            }

            timer = null;
        }

        for (final javax.swing.JComponent c : components.values()) {
            if (c instanceof javax.swing.JComboBox) {
                ((javax.swing.JComboBox<?>)c).removeItemListener(this);
            } else if (c instanceof javax.swing.JSpinner) {
                ((javax.swing.JSpinner)c).removeChangeListener(this);
            } else if (c instanceof javax.swing.JSlider) {
                ((javax.swing.JSlider)c).removeChangeListener(this);
            }
        }

        if (piano != null) {
            piano.dispose();
            piano = null;
        }

        if (components != null) {
            components.clear();
            components = null;
        }

        dialog = null;
        bundle = null;

        player.dispose();
        super.dispose();
    }
}
