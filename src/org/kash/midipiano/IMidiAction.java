package org.kash.midipiano;

/**
 * This interface is a callbackinterface for MIDI note events.
 */
interface IMidiAction
{
    /**
     * Constants for midiEvent.
     */
    int MIDI_PROGRAM_CHANGE = 3;
    int MIDI_TEMPO_CHANGE = 2;
    int MIDI_NOTE_OFF = 1;
    int MIDI_NOTE_ON = 0;

    /**
     * Callback for a PianoKey mousepress event.
     *
     * @param note The midi note to play.
     */
    void pianoMousePressedEvent(final int note);

    /**
     * Callback for a PianoKey mouseup event.
     *
     * @param note The midi note to release.
     */
    void pianoMouseReleasedEvent(final int note);

    /**
     * Callback for MIDI events during playback.
     *
     * @param type The MIDI event type.
     * @param value The MIDI value.
     */
    void playbackMIDIEvent(final int type, final int value);

    /**
     * Callback for statechanges.
     *
     * @param state The new state the player is in.
     */
    void playerStateChange(final int state);

    /**
     * Callback method when a file is succesfully openend.
     */
    void playerFileOpenened(final java.io.File file);
}
