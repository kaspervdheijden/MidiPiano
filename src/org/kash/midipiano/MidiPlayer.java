package org.kash.midipiano;

import javax.sound.midi.InvalidMidiDataException;
import java.io.IOException;

/**
 * Provides Midi playback for a MIDI file, and for direct acces to a synthesizer.
 */
@SuppressWarnings("unused")
final class MidiPlayer implements javax.sound.midi.Receiver, javax.sound.midi.MetaEventListener
{
    private final class ScheduledPlaybackStarter implements java.lang.Runnable
    {
        @Override
        public void run()
        {
            play();
        }
    }

    private static final int META_DONE_PLAYING = 47;
    private static final int META_TYPE_TEMPO = 0x51;
    private static final int CHANGE_VOLUME = 7;

    private static final java.lang.String DEFAULT_TIME = "0:00";
    private static final java.lang.String DEFAULT_DURATION = DEFAULT_TIME + " / " + DEFAULT_TIME;
    private static final int DEFAULT_LOADNESS = 96;

    static final int STATE_PLAYING = 1;
    static final int STATE_PAUSED = 2;
    static final int STATE_IDLE = 0;

    static final int MAX_TEMPO = 255;
    static final int MIN_TEMPO = 16;

    static final int DEFAULT_INSTRUMENT = 0;
    static final int DEFAULT_TEMPO = 120;

    private java.lang.String totalLengthSeconds = DEFAULT_TIME;
    private javax.sound.midi.Synthesizer synthesizer = null;
    private javax.sound.midi.Sequencer sequencer = null;
    private javax.sound.midi.MidiChannel channel = null;
    private javax.sound.midi.Sequence sequence = null;
    private IMidiAction listener = null;
    private boolean perpetual = false;
    private int state = STATE_IDLE;
    private int totalLength = 0;

    private MidiPlayer()
    {
        super();
    }

    /**
     * Creates a MidiPlayer.
     *
     * @return The created MidiPlayer.
     * @throws javax.sound.midi.MidiUnavailableException If Midi cannot be initialized.
     */
    static MidiPlayer create() throws javax.sound.midi.MidiUnavailableException
    {
        final javax.sound.midi.Sequencer seq     = javax.sound.midi.MidiSystem.getSequencer(false);
        final javax.sound.midi.Synthesizer synth = javax.sound.midi.MidiSystem.getSynthesizer();
        final java.lang.String SOUNDBANK_URL     = "resources/soundbank.gm";

        if (synth == null || seq == null) {
            throw new javax.sound.midi.MidiUnavailableException();
        }

        synth.open();
        seq.open();

        // Look for the first available MIDI channel..;
        javax.sound.midi.MidiChannel midiChannel = null;
        for (final javax.sound.midi.MidiChannel ch : synth.getChannels()) {
            if (ch != null) {
                midiChannel = ch;
                break;
            }
        }

        if (midiChannel == null) {
            throw new javax.sound.midi.MidiUnavailableException();
        }

        // Ensure a soundbank is loaded. Default Java SE installation may not include a soundbank.
        //noinspection ConstantValue,LoopStatementThatDoesntLoop
        do {
            javax.sound.midi.Soundbank sndBank = synth.getDefaultSoundbank();

            // First check the default soundbank..;
            if (sndBank  != null && synth.isSoundbankSupported(sndBank)) {
                if (sndBank.getInstruments().length > 0) {
                    synth.loadAllInstruments(sndBank);
                    break;
                }
            }

            final java.io.InputStream sndBankInput = Application.class.getResourceAsStream(SOUNDBANK_URL);
            try {
                if (sndBankInput != null) {
                    sndBank = javax.sound.midi.MidiSystem.getSoundbank(sndBankInput);

                    if (sndBank != null&& synth.isSoundbankSupported(sndBank)) {
                        synth.loadAllInstruments(sndBank);

                        if (sndBank.getInstruments().length > 0) {
                            break;
                        }
                    }
                }
            } catch (final InvalidMidiDataException | IOException err) {
                Application.handleException(err);
            } finally {
                if (sndBankInput != null) {
                    try {
                        sndBankInput.close();
                    } catch (final java.io.IOException err) {
                        Application.handleException(err);
                    }
                }
            }

            throw new javax.sound.midi.MidiUnavailableException();
        } while (false);

        final MidiPlayer player = new MidiPlayer();

        seq.getTransmitter().setReceiver(synth.getReceiver());
        seq.getTransmitter().setReceiver(player);
        seq.addMetaEventListener(player);

        player.channel     = midiChannel;
        player.synthesizer = synth;
        player.sequencer   = seq;

        return player;
    }

    /**
     * Opens a MIDI file for playback.
     *
     * @param file                                          The midi filename to load. Null values will be ignored.
     *
     * @throws javax.sound.midi.InvalidMidiDataException If there is an error loading the sequence file.
     * @throws java.io.IOException                          If there is an IO exception while loading the file.
     */
    void open(final java.io.File file) throws javax.sound.midi.InvalidMidiDataException,
                                              java.io.IOException
    {
        if (file == null) {
            return;
        }

        final javax.sound.midi.Sequence seq = javax.sound.midi.MidiSystem.getSequence(file);
        if (seq == null) {
            return;
        }

        final long len = seq.getTickLength();
        if (len > java.lang.Integer.MAX_VALUE) {
            throw new javax.sound.midi.InvalidMidiDataException();
        } else {
            stop();

            sequence = seq;
            sequencer.setSequence(sequence);

            totalLengthSeconds = formatTime(seq.getMicrosecondLength());
            totalLength        = (int)len;

            listener.playerFileOpenened(file);
        }
    }

    /**
     * Sets the UI listener for midi events.
     *
     * @param uiListener The callback handler.
     */
    void setUIListener(final IMidiAction uiListener)
    {
        listener = uiListener;
    }

    /**
     * Gets the current playback position.
     *
     * @return The position.
     */
    int getPosition()
    {
        return sequence == null ? 0 : (int)sequencer.getTickPosition();
    }

    /**
     * Gets the total length of the currently loaded sequence, or 0 if none.
     *
     * @return The total length.
     */
    int getLength()
    {
        return totalLength;
    }

    /**
     * Sets the current playback position.
     *
     * @param position The requested playback position.
     */
    void setPosition(final int position)
    {
        if (sequence != null) {
            sequencer.setTickPosition(position);
        }
    }

    /**
     * Gets a value if this sequence is playable.
     *
     * @return true if we're playable, false if not.
     */
    boolean isPlayable()
    {
        return totalLength > 0;
    }

    /**
     * Turns perpetual playback on or off.
     *
     * @param newPerpetual A value to turn perpetual playback on/off.
     */
    void setPerpetual(final boolean newPerpetual)
    {
        perpetual = newPerpetual;
    }

    /**
     * Returns a value if we're currently playing.
     *
     * @return true if we are, false if we aren't.
     */
    boolean isPlaying()
    {
        return STATE_PLAYING == state;
    }

    /**
     * Gets the user-friendly display string for current position / total time.
     *
     * @return The display string.
     */
    java.lang.String getPositionTimeDisplayString()
    {
        return sequencer == null || sequence == null ?
                DEFAULT_DURATION :
                formatTime(sequencer.getMicrosecondPosition()) + " / " + totalLengthSeconds;
    }

    private java.lang.String formatTime(final long value)
    {
        final java.lang.String TIME_SEPERATOR = ":";

        final int total       = (int) (value / 1000000);
        final int hours       = total / 3600;
        final int seconds     = total % 60;
        final int minutes     = total / 60;
        java.lang.String time = "";

        if (0 < hours) {
            time = hours + TIME_SEPERATOR;

            if (0 < minutes && 10 > minutes) {
                time += "0";
            }
        }

        time += minutes + TIME_SEPERATOR;
        if (10 > seconds) {
            time += "0";
        }

        return time + seconds;
    }

    void pause()
    {
        if (sequence != null && isPlaying()) {
            sequencer.stop();
            listener.playerStateChange(state = STATE_PAUSED);
        }
    }

    /**
     * Stops playback. Sets playback position to 0.
     */
    void stop()
    {
        if (sequence != null) {
            if (isPlaying()) {
                sequencer.stop();
            }

            sequencer.setTickPosition(0L);
            listener.playerStateChange(state = STATE_IDLE);
        }
    }

    void play()
    {
        if (sequence != null && ! isPlaying() && isPlayable()) {
            sequencer.start();
            state = STATE_PLAYING;
            listener.playerStateChange(state);
        }
    }

    /**
     * Sets the current playback instrument.
     *
     * @param instrument The instrument to change to.
     */
    void setInstrument(final int instrument)
    {
        if (sequence != null && ! isPlaying()) {
            for (final javax.sound.midi.Track track : sequence.getTracks()) {
                final int size = track.size();

                for (int i = 0; i < size; ++i) {
                    final javax.sound.midi.MidiEvent ev  = track.get(i);
                    final javax.sound.midi.MidiMessage m = ev.getMessage();

                    if (0 < ev.getTick()) {
                        // Only set initial instruments..!
                        break;
                    } else if (m instanceof final javax.sound.midi.ShortMessage msg) {
                        if (javax.sound.midi.ShortMessage.PROGRAM_CHANGE == msg.getCommand()) {
                            if (msg.getData1() != instrument) {
                                try {
                                    msg.setMessage(javax.sound.midi.ShortMessage.PROGRAM_CHANGE, msg.getChannel(), instrument, msg.getData2());
                                } catch (final javax.sound.midi.InvalidMidiDataException err) {
                                    Application.handleException(err);
                                }
                            }
                        }
                    }
                }
            }
        }

        setProgram(instrument);
    }

    /**
     * Sets all playback tempos to the desired tempo. This method removes all
     * tempo changes in the sequence.
     *
     * @param tempo The desired tempo in BPM (Beats Per Minute).
     */
    void setAllTempos(final int tempo)
    {
        if (sequence != null && ! isPlaying() && sequencer.getTempoInBPM() != tempo) {
            for (final javax.sound.midi.Track track : sequence.getTracks()) {
                final int size = track.size();

                for (int i = size - 1; i > -1; --i) {
                    final javax.sound.midi.MidiEvent ev  = track.get(i);
                    final javax.sound.midi.MidiMessage m = ev.getMessage();

                    if (m instanceof javax.sound.midi.MetaMessage) {
                        if (META_TYPE_TEMPO == ((javax.sound.midi.MetaMessage)m).getType()) {
                            track.remove(ev);
                        }
                    }
                }
            }

            setTempoImpl(tempo);
        }
    }

    private void setTempoImpl(final int tempo)
    {
        sequencer.setTempoFactor(1.0f); // sequencer.setTempoFactor(tempo / fCurrent);
        sequencer.setTempoInBPM(tempo);

        totalLengthSeconds = formatTime(sequencer.getMicrosecondLength());
        totalLength = (int)sequence.getTickLength();
    }

    /**
     * Sets the current playback tempo.
     *
     * @param tempo The tempo to change to.
     */
    void setTempo(final int tempo)
    {
        if (sequence != null && !isPlaying() && sequencer.getTempoInBPM() != tempo) {
            setTempoImpl(tempo);
        }
    }

    /**
     * Gets to current playback tempo of the sequence. Returns DEFAULT_TEMPO if
     * none loaded.
     *
     * @return The current tempo.
     */
    int getTempo()
    {
        return sequence == null ? DEFAULT_TEMPO : (int)sequencer.getTempoInBPM();
    }

    /**
     * Gets the instrument names, combobox friendly.
     *
     * @return Array of instrument names.
     */
    java.lang.String[] getInstruments()
    {
        final javax.sound.midi.Instrument[] instruments = synthesizer.getLoadedInstruments();
        final int size                                  = Math.min(instruments.length, 127);
        final java.lang.String[] results                = new java.lang.String[size];

        for (int i = 0; i < size; ++i) {
            results[i] = 1 + i + ". " + instruments[i].getName();
        }

        return results;
    }

    void setVolume(final int volume)
    {
        if (synthesizer == null) {
            return;
        }

        final javax.sound.midi.MidiChannel[] channels = synthesizer.getChannels();
        if (channels == null) {
            return;
        }

        for (javax.sound.midi.MidiChannel midiChannel : channels) {
            midiChannel.controlChange(CHANGE_VOLUME, volume);
        }
    }

    /**
     * Sets playback volume by percentage.
     *
     * @param percentage Volume playback percentage. Value must be between 0 and 100.
     */
    void setVolumePercentage(final int percentage)
    {
        setVolume(((int)(1.27f * percentage)) & 127);
    }

    /**
     * Returns the currently selected instrument on the synthesizer.
     *
     * @return The current instrument program number.
     */
    int getProgram()
    {
        return channel == null ? 0 : channel.getProgram();
    }

    /**
     * Changes the synthesizers instrument.
     */
    void setProgram(final int instrument)
    {
        if (channel != null) {
            channel.programChange(instrument);
        }
    }

    /**
     * Turns a note on.
     *
     * @param note The note to turn on.
     */
    void noteOn(final int note)
    {
        if (channel != null) {
            channel.noteOn(note, DEFAULT_LOADNESS);
        }
    }

    /**
     * Turns a note off.
     *
     * @param note The note to turn off.
     */
    void noteOff(final int note)
    {
        if (channel != null) {
            channel.noteOff(note, DEFAULT_LOADNESS);
        }
    }

    /**
     * Schedules a playback after the event code finishes.
     */
    void schedulePlaybackStart()
    {
        java.awt.EventQueue.invokeLater(new ScheduledPlaybackStarter());
    }

    @Override
    public void meta(final javax.sound.midi.MetaMessage meta)
    {
        if (META_DONE_PLAYING != meta.getType()) {
            return;
        }

        sequencer.setTickPosition(0);
        if (perpetual) {
            stop();
            schedulePlaybackStart();
        } else {
            state = STATE_IDLE;
            listener.playerStateChange(state);
        }
    }

    @Override
    public void close()
    {
    }

    @Override
    public void send(final javax.sound.midi.MidiMessage message, final long timeStamp)
    {
        if (STATE_PLAYING != state) {
            return;
        }

        try {
            switch (message.getStatus() & 0xF0) {
                case javax.sound.midi.ShortMessage.NOTE_ON -> {
                    if (0 == message.getMessage()[2]) {
                        listener.playbackMIDIEvent(IMidiAction.MIDI_NOTE_OFF, message.getMessage()[1]);
                    } else {
                        listener.playbackMIDIEvent(IMidiAction.MIDI_NOTE_ON, message.getMessage()[1]);
                    }
                }
                case javax.sound.midi.ShortMessage.NOTE_OFF ->
                    listener.playbackMIDIEvent(IMidiAction.MIDI_NOTE_OFF, message.getMessage()[1]);
                case javax.sound.midi.ShortMessage.PROGRAM_CHANGE -> {
                    final int program = ((javax.sound.midi.ShortMessage)message).getData1();

                    setProgram(program);
                    listener.playbackMIDIEvent(IMidiAction.MIDI_PROGRAM_CHANGE, program);
                }
                case META_TYPE_TEMPO ->
                    listener.playbackMIDIEvent(IMidiAction.MIDI_TEMPO_CHANGE, (int)sequencer.getTempoInBPM());
            }
        } catch (final java.lang.Exception err) {
            Application.handleException(err);
        }
    }

    void dispose()
    {
        if (sequencer != null) {
            if (sequencer.isOpen() && sequencer.isRunning()) {
                sequencer.stop();
            }

            sequencer.removeMetaEventListener(this);
            sequencer.close();
            sequencer = null;
        }

        if (channel != null) {
            channel.allSoundOff();
            channel = null;
        }

        if (synthesizer != null) {
            synthesizer.close();
            synthesizer = null;
        }

        totalLengthSeconds = null;
        sequence           = null;
    }
}
