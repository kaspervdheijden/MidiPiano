package org.kash.midipiano;

import java.io.Serial;

/**
 * Show information about this application.
 */
final class AboutForm extends javax.swing.JDialog implements java.awt.event.ActionListener
{
    private static final java.lang.String TXT_PROGRAM_VERSION = "TXT_PROGRAM_VERSION";
    private static final java.lang.String TXT_PROGRAM_VENDOR = "TXT_PROGRAM_VENDOR";
    private static final java.lang.String CMD_BUTTON_OK = "CMD_BUTTON_OK";
    private static final java.lang.String CMD_ABOUT = "CMD_ABOUT";

    @Serial
    private static final long serialVersionUID = 1L;

    private javax.swing.JButton closeButton;
    private java.awt.Image image;

    private AboutForm(final javax.swing.JFrame parent, final java.util.ResourceBundle bundle)
    {
        super(parent, bundle.getString(CMD_ABOUT).replace('.', ' ').trim() + ": " + Application.getName(), true);

        final javax.swing.JLabel versionLabel    = new javax.swing.JLabel(bundle.getString(TXT_PROGRAM_VERSION));
        final javax.swing.JLabel vendorLabel     = new javax.swing.JLabel(bundle.getString(TXT_PROGRAM_VENDOR));
        final javax.swing.JLabel appVersionLabel = new javax.swing.JLabel(Application.getVersion());
        final javax.swing.JLabel appVendorLabel  = new javax.swing.JLabel(Application.getVendor());
        final javax.swing.JLabel appTitleLabel   = new javax.swing.JLabel(Application.getName());
        final javax.swing.GroupLayout layout     = new javax.swing.GroupLayout(getContentPane());
        final java.awt.Font defaultFont          = appTitleLabel.getFont();

        closeButton = new javax.swing.JButton(bundle.getString(CMD_BUTTON_OK));
        image       = parent.getIconImage();

        appTitleLabel.setFont(defaultFont.deriveFont(defaultFont.getStyle() | java.awt.Font.BOLD, 4 + defaultFont.getSize()));
        versionLabel.setFont(defaultFont.deriveFont(defaultFont.getStyle() | java.awt.Font.BOLD));
        vendorLabel.setFont(defaultFont.deriveFont(defaultFont.getStyle() | java.awt.Font.BOLD));

        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                layout.createSequentialGroup().addGap(110, 110, 110).addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING).addGroup(
                javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup().addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(
                versionLabel).addComponent(vendorLabel)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(appVersionLabel).addComponent(
                appVendorLabel))).addComponent(appTitleLabel, javax.swing.GroupLayout.Alignment.LEADING).addComponent(closeButton)).addContainerGap(30, 30)));
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                layout.createSequentialGroup().addContainerGap().addComponent(appTitleLabel).addPreferredGap(
                javax.swing.LayoutStyle.ComponentPlacement.RELATED).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(versionLabel).addComponent(
                appVersionLabel)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(
                javax.swing.GroupLayout.Alignment.BASELINE).addComponent(vendorLabel).addComponent(appVendorLabel)).addPreferredGap(
                javax.swing.LayoutStyle.ComponentPlacement.RELATED).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 19,
                Short.MAX_VALUE).addComponent(closeButton).addContainerGap()));

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getRootPane().setDefaultButton(closeButton);
        setResizable(false);
        setLayout(layout);
        pack();

        setLocationRelativeTo(parent);
    }

    /**
     * Displays the form. This method should be called when this form is to be displayed.
     */
    static void showForm(final javax.swing.JFrame parent, final java.util.ResourceBundle bundle)
    {
        final AboutForm form = new AboutForm(parent, bundle);
        form.closeButton.addActionListener(form);

        form.setVisible(true);
    }

    @Override
    public void actionPerformed(final java.awt.event.ActionEvent e)
    {
        dispose();
    }

    @Override
    public void paint(final java.awt.Graphics g)
    {
        super.paint(g);

        final java.awt.Image img = image;
        if (img == null) {
            return;
        }

        for (int x = 0; x < 80; x += 20) {
            for (int y = 0; y < 60; y += 20) {
                g.drawImage(img, 18 + x, 40 + y, null);
            }
        }
    }

    @Override
    public void dispose()
    {
        if (closeButton != null) {
            closeButton.removeActionListener(this);
            closeButton = null;
        }

        image = null;
        super.dispose();
    }
}
