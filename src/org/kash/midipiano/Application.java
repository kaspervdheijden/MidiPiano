package org.kash.midipiano;

import javax.swing.*;
import javax.swing.plaf.FontUIResource;

final class Application implements java.lang.Runnable
{
    private static final java.lang.String APP_VENDOR = "Kasper van der Heijden (KasH.)";
    private static final java.lang.String APP_NAME = "Midi Piano";
    private static final java.lang.String APP_VERSION = "2.2.0";
    private static final boolean APP_DEBUG = false;

    private static final java.lang.String DEFAULT_ERR_INIT_LANG_MSG = "Error initiating language data. Cannot continue.";
    private static final java.lang.String BUNDLE_PACKAGE_BASENAME = "org/kash/midipiano/resources/lang";

    // Constants to use for exit();
    static final int EXITCODE_ERROR = 1;

    static final java.lang.String ERR_INVALID_MIDI_DATA = "ERR_INVALID_MIDI_DATA";
    static final java.lang.String ERR_MIDI_UNAVAILABLE = "ERR_MIDI_UNAVAILABLE";

    private final java.util.ResourceBundle bundle;
    private final java.lang.String[] arguments;
    private final MidiPlayer player;

    private Application(
        final MidiPlayer midiPlayer,
        final java.util.ResourceBundle resourceBundle,
        final java.lang.String[] args
    ) {
        super();

        bundle    = resourceBundle;
        player    = midiPlayer;
        arguments = args;
    }

    /**
     * Gets the application name.
     *
     * @return The application name.
     */
    static java.lang.String getName()
    {
        return APP_NAME;
    }

    /**
     * Gets the application vendor.
     *
     * @return The application vendor.
     */
    static java.lang.String getVendor()
    {
        return APP_VENDOR;
    }

    /**
     * Gets the application version.
     *
     * @return The application version.
     */
    static java.lang.String getVersion()
    {
        return APP_VERSION;
    }

    /**
     * Exists the application with generic error code.
     */
    static void exitFail()
    {
        System.exit(EXITCODE_ERROR);
    }

    /**
     * Displays an error message to the user.
     *
     * @param parent  The parent form.
     * @param message The message to show.
     */
    static void showErrorMessage(final java.awt.Component parent, final java.lang.String message)
    {
        javax.swing.JOptionPane.showMessageDialog(parent, message, APP_NAME, javax.swing.JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Handle any silent exceptions.
     *
     * @param err The unexpected Exception that was thrown.
     */
    static void handleException(final java.lang.Exception err)
    {
        if (APP_DEBUG && err != null && System.err != null) {
            err.printStackTrace(System.err);
        }
    }

    @Override
    public void run()
    {
        java.io.File file = null;

        // Parse commandline..;
        for (String arg : arguments) {
            if (arg.isEmpty()) {
                continue;
            }

            file = new java.io.File(arg);
            if (file.exists()) {
                break;
            } else {
                file = null;
            }
        }

        AppForm.showForm(APP_NAME, bundle, player, file);
    }

    /**
     * The program entry point.
     *
     * @param args the commandline arguments.
     */
    public static void main(final java.lang.String[] args)
    {
        java.util.ResourceBundle resourceBundle;
        MidiPlayer midiPlayer;

        try {
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());

            java.util.Enumeration<Object> keys = UIManager.getDefaults().keys();
            while (keys.hasMoreElements()) {
                Object key   = keys.nextElement();
                Object value = UIManager.get(key);

                if (value instanceof FontUIResource curFont) {
                    javax.swing.plaf.FontUIResource newFont = new javax.swing.plaf.FontUIResource(
                        curFont.getName(),
                        curFont.getStyle(),
                        (int) (curFont.getSize() * 1.3)
                    );

                    UIManager.put(key, newFont);
                }
            }
        } catch (final ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException err) {
            handleException(err);
        }

        try {
            resourceBundle = java.util.PropertyResourceBundle.getBundle(BUNDLE_PACKAGE_BASENAME);
        } catch (final java.util.MissingResourceException err) {
            handleException(err);

            showErrorMessage(null, DEFAULT_ERR_INIT_LANG_MSG);
            exitFail();

            return;
        }

        if (resourceBundle == null) {
            showErrorMessage(null, DEFAULT_ERR_INIT_LANG_MSG);
            exitFail();

            return;
        }

        try {
            midiPlayer = MidiPlayer.create();
        } catch (final javax.sound.midi.MidiUnavailableException err) {
            handleException(err);

            javax.swing.JOptionPane.showMessageDialog(null, DEFAULT_ERR_INIT_LANG_MSG, APP_NAME, javax.swing.JOptionPane.ERROR_MESSAGE);
            showErrorMessage(null, resourceBundle.getString(ERR_MIDI_UNAVAILABLE));
            exitFail();

            return;
        }

        for (final java.lang.String key : new java.lang.String[] {
                "acceptAllFileFilterText", "lookInLabelText", "filesOfTypeLabelText", "upFolderToolTipText", "fileNameLabelText",
                "homeFolderToolTipText", "newFolderToolTipText", "listViewButtonToolTipTextlist", "detailsViewButtonToolTipText",
                "saveButtonText", "openButtonText", "cancelButtonText", "updateButtonText", "helpButtonText",
                "saveButtonToolTipText", "openButtonToolTipText", "fileSizeHeaderText", "fileDateHeaderText",
                "fileTypeHeaderText", "fileAttrHeaderText", "openDialogTitleText", "saveDialogTitleText",
                "cancelButtonToolTipText", "updateButtonToolTipText", "helpButtonToolTipText"}) {
            try {
                javax.swing.UIManager.put("FileChooser." + key, resourceBundle.getString(key));
            } catch (final java.util.MissingResourceException err) {
                handleException(err);
            }
        }

        java.awt.EventQueue.invokeLater(new Application(midiPlayer, resourceBundle, args));
    }
}
