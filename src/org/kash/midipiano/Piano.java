package org.kash.midipiano;

import java.io.Serial;

/**
 * Represents a UI Piano.
 */
final class Piano extends javax.swing.JComponent
{
    /**
     * Represents a single pianokey.
     */
    private final class PianoKey extends javax.swing.JPanel implements java.awt.event.MouseListener
    {
        @Serial
        private static final long serialVersionUID = 1L;

        private final java.awt.Color defaultColor;
        private int pressCount = 0;
        private final int note;

        /**
         * @param midiNote The Midinote associated with this pianokey.
         * @param defColor Its background color, java.awt.Color.WHITE for a white key and java.awt.Color.Black for a bloack key.
         * @param x        The x coordination for this component.
         * @param y        The y coordination for this component.
         * @param w        The width for this component.
         * @param h        The height for this component.
         */
        PianoKey(
            final int midiNote,
            final java.awt.Color defColor,
            final int x,
            final int y,
            final int w,
            final int h
        ) {
            super(true);

            setBackground(defaultColor = defColor);
            setBounds(x, y, w, h);
            note = midiNote;
        }

        @Override
        protected void paintComponent(final java.awt.Graphics g)
        {
            final java.awt.Graphics2D gx = (java.awt.Graphics2D) g;
            final int height             = getHeight();
            final int width              = getWidth();

            gx.setRenderingHint(java.awt.RenderingHints.KEY_ANTIALIASING, java.awt.RenderingHints.VALUE_ANTIALIAS_ON);

            gx.setColor(getBackground());
            gx.fillRoundRect(0, 0, width - 1, height - 1, 5, 5);

            gx.setColor(new java.awt.Color(89, 89, 89));
            gx.setStroke(new java.awt.BasicStroke(1));

            gx.drawRoundRect(0, 0, width - 1, height - 1, 5, 5);

            gx.setStroke(new java.awt.BasicStroke());
        }

        /**
         * Presses this piano key.
         */
        void press()
        {
            if (pressCount == 0) {
                setBackground(PRESSED_KEY_COLOR);
                Piano.this.repaint();
                ++numPressed;
            }

            ++pressCount;
        }

        /**
         * Releases this pianokey.
         *
         * @param repaint When <code>true</code>, forces a repaint.
         */
        void release(final boolean repaint)
        {
            if (pressCount <= 0) {
                return;
            }

            setBackground(defaultColor);
            if (repaint) {
                Piano.this.repaint();
                --pressCount;
            } else {
                pressCount = 0;
            }

            if (pressCount == 0) {
                --numPressed;
            }
        }

        @Override
        public void mousePressed(final java.awt.event.MouseEvent e)
        {
            press();

            if (midiListener != null) {
                midiListener.pianoMousePressedEvent(note);
            }
        }

        @Override
        public void mouseReleased(final java.awt.event.MouseEvent e)
        {
            release(true);

            if (midiListener != null) {
                midiListener.pianoMouseReleasedEvent(note);
            }
        }

        @Override
        public void mouseClicked(final java.awt.event.MouseEvent e)
        {
        }

        @Override
        public void mouseEntered(final java.awt.event.MouseEvent e)
        {
        }

        @Override
        public void mouseExited(final java.awt.event.MouseEvent e)
        {
        }
    }

    private static final java.awt.Color PRESSED_KEY_COLOR = new java.awt.Color(240, 240, 159);
    @Serial
    private static final long serialVersionUID = 1L;

    private IMidiAction midiListener;
    private PianoKey[] pianoKeys;
    private final int noteOffset;
    private int numPressed = 0;
    private int width = 0;

    /**
     * @param numberOfOctaves The number of initial octaves this piano should have. If the number is odd, one will be added.
     * @param listener        A listener for pressing and releasing callbacks.
     * @param x               The location x point.
     * @param y               The location y point.
     */
    Piano(final int numberOfOctaves, final IMidiAction listener, final int x, final int y)
    {
        super();

        final int[] WHITE_KEYS = {0, 2, 4, 5, 7, 9, 11};
        final int[] BLACK_KEYS = {1, 3, 6, 8, 10};
        final int numKeys      = 1 + (numberOfOctaves * 12);

        noteOffset   = 60 - (12 * (numberOfOctaves / 2));
        pianoKeys    = new PianoKey[numKeys];
        midiListener = listener;

        for (int i = 0; i < numberOfOctaves; ++i) {
            int inner  = 17 + width;
            int offset = 12 * i;

            for (int j = 0; j < 5; ++j) {
                add(pianoKeys[offset + BLACK_KEYS[j]] = new PianoKey(offset + BLACK_KEYS[j] + noteOffset, java.awt.Color.DARK_GRAY, inner, 0, 20, 60));
                inner += 1 == j ? 56 : 28;
            }

            for (int j = 0; j < 7; ++j) {
                add(pianoKeys[offset + WHITE_KEYS[j]] = new PianoKey(offset + WHITE_KEYS[j] + noteOffset, java.awt.Color.WHITE, width, 0, 27, 100));
                width += 28;
            }
        }

        add(pianoKeys[numKeys - 1] = new PianoKey(numKeys - 1 + noteOffset, java.awt.Color.WHITE, width, 0, 27, 100));
        setSize(new java.awt.Dimension(40 + width, 100));
        setLocation(x, y);

        for (int i = numKeys; --i > -1;) {
            pianoKeys[i].addMouseListener(pianoKeys[i]);
        }
    }

    @Override
    public int getWidth()
    {
        return 40 + width;
    }

    /**
     * Presses a piano key.
     *
     * @param note The midi note for which the key should be pressed for.
     */
    void pressKey(final int note)
    {
        pianoKeys[note - noteOffset].press();
    }

    /**
     * Releases a specific piano key.
     *
     * @param note The midi note for which the key should be released for.
     */
    void releaseKey(final int note)
    {
        pianoKeys[note - noteOffset].release(true);
    }

    /**
     * Releases all keys.
     */
    void releaseAllKeys()
    {
        for (int i = 0, n = pianoKeys.length; numPressed > 0 && i < n; ++i) {
            pianoKeys[i].release(false);
        }

        repaint();
    }

    /**
     * Disposes and releases all resources used by this class.
     */
    void dispose()
    {
        if ( pianoKeys != null) {
            for (int i = 0, n = pianoKeys.length; i < n; ++i) {
                if (null != pianoKeys[i]) {
                    pianoKeys[i].removeMouseListener(pianoKeys[i]);
                    pianoKeys[i] = null;
                }
            }

            pianoKeys = null;
        }

        if (midiListener != null) {
            midiListener = null;
        }

        removeAll();
    }
}
